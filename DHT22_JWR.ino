/// Original demo
/// Demo readout of the DHT11 or DHT22 temperature/humidity sensors.
/// @see http://jeelabs.org/2012/04/14/reading-out-dhtxx-sensors/

/// DHT22 pin 1 VCC, between pin 1,2 10K brouwn, black orange
///       pin 3 NC, pin 4 GND
/// Pin 2 -> DIO

/// GY-65 Plug (Bosh BMP085)
/// http://www.vanallesenmeer.nl/Webwinkel-Product-38495759/Sensor-Barometrische-druk-op-breakoutboard-Bosch-BMP085.html
/// Board ->  JeeNode
/// VCC(1) -> VCC(4)
/// SDA(2) -> DIO(2)
/// SCL(3) -> AIO(5)
/// XCLR(4) NC
/// EOC(5)  NC
/// GND(6) -> GND(3)

#include <JeeLib.h>
#include <PortsBMP085.h>

DHTxx dht (5); // connected to DIO2
PortI2C port1 (1);
BMP085 pressure (port1);


void setup () {
  Serial.begin(57600);
  Serial.println("\n[dht_demo]");
  pressure.getCalibData(); 
}

void loop () {
  int temp;
  long pres;
  pressure.measure(BMP085::TEMP);
  pressure.measure(BMP085::PRES);
  pressure.calculate(temp,pres);

Serial.println("BMP085");
  Serial.print(" Pressure (hPa) = ");
  Serial.println(pres*0.01);
  Serial.print(" Temperature (C) = ");
  Serial.println(temp*0.1);

  int t, h;
  if (dht.reading(t, h, true)) {
    Serial.println("DHT22");
    Serial.print(" Humidity (%) = ");
    Serial.println(h*0.1);
    Serial.print(" Temperature (C) = ");
    Serial.println(t*0.1);
    Serial.println();
  }
  delay(3000);
}
